class Animal{
    eat = () => console.log('A am eating')
}

class Dog extends Animal{
    weight = 20;
    age = 8;
    bark = () => console.log('bark bark bark')
}

puppy = new Dog();

puppy.bark()
puppy.eat()