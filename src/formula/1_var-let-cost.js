// create to var
console.log("to var");
var Age = 13;

if(true){
    var Age = 98;
    console.log(`to var = ${Age}`);
}

console.log(`to var = ${Age}`);

// create to let
console.log("");
console.log("to let");
let Ages = 11;

if(true){
    let Ages = 80;
    console.log(`to let = ${Ages}`);
}

console.log(`to let = ${Ages}`);

console.log("");
console.log("to const obj");
const user = {}
    user.name = "mick"
    console.log(user);

console.log("");
console.log("to const array");
const fruit = ["orange"]
    fruit.push("apple")
    console.log(fruit);    
