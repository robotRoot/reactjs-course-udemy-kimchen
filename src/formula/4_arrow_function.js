// example arrow function 1
function sayMyName(name){
    return `My Name is ${name}`
}

console.log(sayMyName("Peter"))

// example arrow function 2
const sayMyName2 = (name) =>{
    return `My Name is ${name}`
}

console.log(sayMyName2("sam"))

// example arrow function 3
const sayMyName3 = name => `My Name is ${name}`
console.log(sayMyName3("may"));