// create var switch in var

function varFruitColor(fruit){
    switch(fruit){
        case "apple":
            var color = "red"
                return color
        case "banana":
            var color = "yellow"
                return color
        default:
            return "other color"
    }
}
console.log("switch case on var");
console.log(varFruitColor("apple"))
console.log(varFruitColor("banana"))
console.log(varFruitColor("other color"))
console.log();


function letFruitColor(fruit){
    switch(fruit){
        case "orange":{
            let color = "orange"
                return color
            }
        case "banana":{
            let color = "red"
                return color
            }
        default:{
            return "other color"
        }    
    }
}
console.log("switch case on let");
console.log(letFruitColor("orange"));
console.log(letFruitColor("red"));
console.log(letFruitColor("other color"));