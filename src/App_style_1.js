import './App.css';

// ========================= > 16. css
// external css
// import './Custom.css';

// ================================= > 18. Styles object
const styles = {
  outerDiv:{padding: '70px 0', backgroundColor: 'blue'},
  innerDiv:{
    textAlign: 'center',
    margin: 'auto',
    width: '60%',
    border: '3px solid black',
    backgroundColor: '#73AD21',
    padding: '70px 0'
  }
}

    // ================================= > 21. Store jsx element to variable
const sometext = 'sometext';
const somediv = <div style={{backgroundColor:'white'}}>somediv</div>;

function App_style_1() {
  return (

    // ================================= > 21. Store jsx element to variable
    <div style={styles.outerDiv}>
    <div style={styles.innerDiv}>
      {somediv}
    </div>
  </div>

    // ================================= > 18. Styles object
    // <div style={styles.outerDiv}>
    //   <div style={styles.innerDiv}>
    //     This is a green box
    //   </div>
    // </div>

    // ==========================================> 17. inline css
    // <div style={{ padding: '70px 0', backgroundColor: 'red'}}>
    //   <div style={{textAlign: 'center', color: 'white'}}>
    //     This is a green box
    //   </div>
    // </div>

    // ===============================> 16. css
    // <div className='outerdiv'>
    //   <div className='innerdiv'>
    //     This is a green box
    //   </div>
    // </div>

  );
}

export default App_style_1;
